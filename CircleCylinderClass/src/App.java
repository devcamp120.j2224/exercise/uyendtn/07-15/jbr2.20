public class App {
    public static void main(String[] args) throws Exception {
        //4.create 3 objects circle 
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green");
        System.out.println(circle1.toString());
        System.out.println(circle2.toString());
        System.out.println(circle3.toString());
        System.out.println("area of circle 1: " + circle1.getArea());
        System.out.println("area of circle 2: " + circle2.getArea());
        System.out.println("area of circle 3: " + circle3.getArea());

        //5.create 4 objects cylinder
        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        Cylinder cylinder4 = new Cylinder(3.5, "green", 1.5);
        System.out.println(cylinder1.toString());
        System.out.println(cylinder2.toString());
        System.out.println(cylinder3.toString());
        System.out.println(cylinder4.toString());
        System.out.println("area of cylinder 1: " + cylinder1.getArea());
        System.out.println("area of cylinder 2: " + cylinder2.getArea());
        System.out.println("area of cylinder 3: " + cylinder3.getArea());
        System.out.println("area of cylinder 4: " + cylinder4.getArea());
}
}
